Usage: SubReTime.exe [name of your .srt file] [time to resync in seconds]

Example: SubReTime.exe 'tv show e1s1.srt' 1

Example: SubReTime.exe 'tv show e1s1.srt' 1.5

Example: SubReTime.exe 'tv show e1s1.srt' -1