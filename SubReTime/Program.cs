﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SubReTime
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileName = args[0];
            if (!double.TryParse(args[1], out double res))
            {
                Console.WriteLine("Error parsing your time argumnet.");
                return;
            }

            var shift = TimeSpan.FromSeconds(res);
            TimeSpan tsFrom, tsTo;

            var file = File.OpenText(fileName);
            var newFile = File.CreateText(fileName.Insert(fileName.LastIndexOf('.'), " - Shifted " + res.ToString()));

            while (!file.EndOfStream)
            {
                var line = file.ReadLine();
                var lineSep = line.Split(' ');

                if (line.Contains("-->") && lineSep.Count() == 3)
                {
                    tsFrom = TimeSpan.ParseExact(lineSep[0], @"hh\:mm\:ss\,fff", null).Add(shift);
                    tsTo = TimeSpan.ParseExact(lineSep[2], @"hh\:mm\:ss\,fff", null).Add(shift);

                    line = tsFrom.ToString(@"hh\:mm\:ss\,fff") + " --> " + tsTo.ToString(@"hh\:mm\:ss\,fff");
                }

                newFile.WriteLine(line);
            }

            file.Close();
            newFile.Flush();
            newFile.Close();
        }
    }
}
